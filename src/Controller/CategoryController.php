<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="category")
     */
    public function index(CategoryRepository $CategoryRepo): Response
    {
        $categories = $CategoryRepo->findAllCategory();

        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
            'categories'=> $categories
        ]);
    }

    
}
